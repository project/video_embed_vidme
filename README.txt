CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module extend the functionality of video embed field for vid.me

REQUIREMENTS
------------

Video Embed Field

INSTALLATION
------------

1. Extract the files in your module directory (typically /modules).
2. Visit the modules page and enable the module.

CONFIGURATION
------------

STEPS:
1. Create a field in your content type of type video embed and widget as video.
2. After that a field will appear in your content type.
3. Just place vid.me video url, of a video in that field.
4. Video will appear in your page of vid.me.

MAINTAINERS
-----------
* Manoj Bisht (manojbisht_drupal) - https://www.drupal.org/u/manojbisht_drupal
* Yogesh Pawar (yogeshmpawar) - https://www.drupal.org/u/yogeshmpawar
* Arjun Kumar (Manav) - https://www.drupal.org/u/manav
